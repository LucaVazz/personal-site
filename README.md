# my personal site


### Powered by
- [11ty](https://www.11ty.dev/) for orchestrating the generation of the static site
- [Sass](https://sass-lang.com/) for style definitions
- [line-awesome](https://icons8.com/line-awesome) for additional icons
- [Signika Negative](http://ancymonic.com/projects/Signika) as a font


---


### Local Development
1. ensure [Node LTS](https://nodejs.org/en/download/) is installed
1. run `npm install -g yarn` to ensure the latest yarn version is installed
1. clone the repository
1. run `yarn` in the cloned dir to install dependencies
1. run `yarn serve` to have a dev server with live reload


### Deployment to OpenShift

For maximum efficiency, a chained build is used.

At first, the site is build in a container and its output from `_site` is taken. Then, a separate container baed on nginx is started to serve the resulting static files.

- To setup deployment for the site, login to the cluster, than run `oc process -f deployment-template.yaml | oc apply -f -`.
- To connect pushes and deployment, configure a web hook in the GitLab repo. Copy the target URL from the settings of `personal-site-integration-build` BuildConfig in OpenShift.
- To apply changes to the template, run the same command.
