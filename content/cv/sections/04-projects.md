---
title: Personal Projects
shade: forest
---

- Experimenting with new and versatile technologies like Vue.js and Flask
- Integrating data in easy to use and helpful ways, for example Deutsche Bahn train information or geo information from Here Maps and Mapbox
- Fulfilling the same high standards for design and ease of use as with my professional projects
- Contributing to open source projects like Sentry and Angular
- Publishing personal projects as open source for the community and as future reference
- More information and examples are available on my portfolio at [*about-me.lucavazzano.eu*](https://about-me.lucavazzano.eu)
