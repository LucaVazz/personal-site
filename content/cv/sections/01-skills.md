---
title: Skills
shade: forest
---

- Proficient in building web applications with JavaScript, TypeScript, Angular and Sass
- Experienced in supporting backend systems with Java and Spring Boot
- Familiar with development using Vue.js, Python, Flask and MongoDB
- Fundamental knowledge of Docker, OpenShift and Linux systems administration
- Expertise in Git as well as Jenkins Pipelines and Code Analysis tools
- Organizing work based on agile principles with Scrum, Kanban and SAFe, with tracking in Jira
- Familiar with distributed and remote teams, highly proficient in German and English
- Experienced in mentoring and taking on technical leadership
