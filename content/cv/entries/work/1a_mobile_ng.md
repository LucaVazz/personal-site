---
tags: cv-work
date: 2019-06-01
endDate: 2019-12-31
title: Development Lead for an Angular migration
subentry: true
---

- Defined and prototyped a new, modern architecture for the Amadeus Mobile web application for rewriting it from AngularJS to Angular
- Helped to assess external contracting companies
- Managed development remotely with teams in Nice and Montpellier
- Agile work organisation based on Scrum
