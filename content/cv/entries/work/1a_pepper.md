---
tags: cv-work
date: 2019-01-21
endDate: 2019-03-22
title: Contributor for a Pepper prototype project
subentry: true
---

- Implemented a prototype app to test a customer concept for helping foreign travellers with the humanoid, automatons robot Pepper
- Development based on Kotlin, the Android SDK and the proprietary Qi SDK
- Achieved in seven weeks with a team of in total seven people
- Continuously refined the implementation based on customer feedback
- Rolled the app out to a one-week field test with every-day travellers, receiving largely positive and delighted reviews
- Demonstrated the app to various potential customers and development partners over the following months 
