---
tags: cv-work
date: 2018-07-02
endDate: 2018-09-30
title: Study on automatic code migrations (Bachelor Thesis)
orgName: Amadeus Germany (Bad Homburg, Germany) and Amadeus France (Nice, France)
orgImage: amadeus.png
---

- Examined the possibility to automatically migrate complex web applications from AngularJS to Angular
- Defined a modular and functionally-oriented tool architecture
- Based the tool on the TypeScript compiler APIs and manipulation of Abstract Syntax Trees
- Evaluated the automated usage against a manual rewrite on a real-life application
