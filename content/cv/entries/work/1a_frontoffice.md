---
tags: cv-work
date: 2020-01-01
endDate: null
title: Full-Stack Contributor for web applications
subentry: true
---

- Maintaining and extending web apps for office management in travel agencies
- Working on both frontend modules written in Angular as well as backend-modules written with Spring Boot
- Contributing to internal libraries and tooling
- Working with a distributed team in Bad Homburg, Nice, Kiev and Bangalore
- Agile work organisation in a SAFe Release Train
