---
tags: cv-work
date: 2018-10-01
endDate: 2019-12-31
title: Development Lead for a web application
subentry: true
---

- Maintained and extended the central web application of the Amadeus Mobile ecosystem, spanning both customer and white-label variants
- Lead and individually contributed to the AngularJS web application
- Mentored new team members
- Worked with a distributed team in Bad Homburg, Nice, Bangalore and Bonn
- Agile work organisation based on Scrum
