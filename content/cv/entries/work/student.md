---
tags: cv-work
date: 2015-08-01
endDate: 2018-06-30
title: Student Contributor for various web applications
orgName: Amadeus Germany (Bad Homburg, Germany) and Amadeus France (Nice, France)
orgImage: amadeus.png
---

- Worked with teams of various web applications of the Amadeus ecosystem, some distributed across Bad Homburg, Nice and Bangalore
- Prototyped internal ideas for automation and process improvements
- Implemented and extended applications using AngularJS, Django, Spring Boot and in select projects with Selenium, Appium, Aurelia and React
- Multiple on-site placements in Nice and project cooperations with various teams
- Agile work organisation based on Scrum
