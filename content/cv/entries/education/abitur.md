---
tags: education
date: 2012-08-13
endDate: 2015-05-13
title: Allgemeine Hochschulreife (Higher Education Qualification)
info: "Grade: 1.5 — Focus: Computing and Electrical Engineering, Mathematics"
orgName: Berufliches Gymansium der Beruflichen Schulen Gross-Gerau
orgImage: bsgg.png
---
