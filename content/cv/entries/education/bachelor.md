---
tags: education
date: 2015-08-01
endDate: 2018-09-30
title: Bachelor of Science in Applied Computer Science
info: "Grade: 1.7 — ECTS-Classification: B — In cooperation with Amadeus Germany GmbH"
orgName: Duale Hochschule Baden-Württemberg Mannheim
orgInfo: Baden-Württemberg Cooperative State University
orgImage: dhbw.png
---
