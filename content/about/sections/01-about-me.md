---
title: about me
shade: forest
---

Hello World! I'm a lead web developer by day and a hobbyist game developer by night, and a maker all the time.

This is my personal showcase. Have a look around and discover my projects.
