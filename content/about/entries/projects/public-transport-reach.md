---
tags: project
date: 2019-07-23
title: Public Transport Reach
image: public-transport-reach.png
git: https://gitlab.com/LucaVazz/public-transport-reach
keywords: 
    - js
    - vue
    - here
    - mapbox
---

Using public transport to get around the city is more environmentally friendly than any other way of transportation. To better explore the reach and ways of public transport, this web-app showed where you can get to from a starting point of your choice. The data was based on Here Maps. Unfortunately the used APIs are now shut down, which is why this web app isn't available anymore.

<br/>

This web-app ran fully client-side and used Vue to orchestrate the interactions. The resulting map was rendered with Mapbox to get a clean and performant vector display.
