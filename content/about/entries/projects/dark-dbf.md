---
tags: project
date: 2018-08-21
title: Dark DBF
image: dbf.png
git: https://gitlab.com/LucaVazz/dark-dbf
_web: https://dbf.lucavazzano.eu/
keywords:
  - py
  - flask
  - bulma
---

This webapp is a custom UI for [dbf.finalrewind.org](https://dbf.finalrewind.org/) that provides a dark design based on Bulma. With it you can get live data for DB trains, which is publicly accessible but hard to get, neatly displayed. Furthermore, it gives travellers additional information like a delay statistic and the wagon order.

<br/>

The server-side, written in Flask, generates dynamic views for the aggregated data and statistics. On the client-side the web-app is fully usable without JavaScript. Additionally, it can be added to the home screen on smartphones for easy, app-like access.
