---
tags: project
date: 2020-04-16
title: Web-Games
image: web-games.png
git: https://gitlab.com/LucaVazz/web-games
_webGame: https://games.lucavazzano.eu/
keywords:
  - py
  - flask
  - mongo
  - bulma
  - sass
  - vue
---

Play some games. With your friends. With friends spread across europe, having an option to play party games virtually is almost a necessity. As I'm obsessed with nice UI design, I decided to create my own web-app for online games.

<br/>

From the technical site, this is achieved with Flask to generate the site content on the server-side. Using web-sockets, all players get instant updates on player moves while Vue is used for some client-side enhancements. For account management, Discord is integrated as an OAuth login. All the data is stored in MongoDB.
