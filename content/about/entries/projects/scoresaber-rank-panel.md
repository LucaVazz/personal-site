---
tags: project
date: 2019-05-19
title: ScoreSaber Rank Panel
image: scoresaber-rank-panel.png
git: https://github.com/LucaVazz/ScoreSaber-Rank-Panel
web: https://dashboard.twitch.tv/extensions/ne4gxyrk26tt1s5s9jaln7w7p9vokc
keywords: 
    - js
    - eleventy
    - twitch
---

While this Twitch extension started as an experiment for a friend, it's currently used by over 500 channels with over 60.000 monthly viewers. With it, streamers who play Beat Saber can provide their viewers with direct information about their statistics and ranking on the community-run leaderboard of ScoreSaber.

<br/>

Building upon Twitch's infrastructure, the extension is written in plain JavaScript, with some help from Eleventy to easily manage the view templates. The first version used web-scraping on a cached version of the leaderboard to get the streamer's statistics. However, as soon as the official API was available, the second version switched to using it instead.
