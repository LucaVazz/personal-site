---
tags: project
date: 2020-07-17
title: This website
image: personal-site.png
git: https://gitlab.com/LucaVazz/personal-site
keywords: 
    - eleventy
    - sass
---

To give my projects a nice home to show them off in a central place, I wrote this site from scratch. As a change, it is fully static content, completely rendered at build time and all animations and interactivity is pure CSS magic.

<br/>

This is realized by using Eleventy to have a modular architecture that separates content and structure. Furthermore, Sass is used to create a fully custom styling. In other words, giving myself a challenge at creating a personal website to show off my web-development skills.
