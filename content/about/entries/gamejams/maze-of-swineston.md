---
tags: gamejam
date: 2022-04-02
title: The Maze of Swineston
image: maze-of-swineston.png
git: https://gitlab.com/green-game-17/the-maze-of-swineston
webGame: https://green-game-17.gitlab.io/the-maze-of-swineston/
keywords:
  - godot
---

[Ludum Dare 50](https://ldjam.com/events/ludum-dare/50/the-maze-of-swineston/)
