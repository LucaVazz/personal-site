---
tags: gamejam
date: 2021-04-27
title: Penguin Paradise
image: penguin-paradise.png
git: https://gitlab.com/green-game-17/penguin-paradise
webGame: https://green-game-17.gitlab.io/penguin-paradise
keywords:
  - godot
---

For [Ludum Dare 48](https://ldjam.com/events/ludum-dare/48/penguin-paradise/), we developed this game about penguins that got stranded in an empty grassland. Together with the player's help, they need to build a thriving village, supporting different kinds of specialized penguins. And if all goes well, the building of a grand monument is rumoured among the penguins.

<br/>

After being rated by the community, our entry is among the top 20% of almost 3 900 submissions, with the voxel graphics scoring our highest rating yet with 3.8 out of 5 stars in the graphics category.

<br/>

Our Godot based game is played directly in a web browser and features a story which guides the player along the penguin's adventure, integrating the tutorial into the game flow.
