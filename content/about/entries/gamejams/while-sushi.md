---
tags: gamejam
date: 2020-10-06
title: while(sushi);
image: while-sushi.png
git: https://gitlab.com/green-game-17/while-sushi
webGame: https://green-game-17.gitlab.io/while-sushi/
keywords:
  - godot
---

Developed during [Ludum Dare 47](https://ldjam.com/events/ludum-dare/47/whilesushi/), this game runs the player through a sushi-making loop, in which they try to make their catstumors happy by preparing a mix of nigiri and maki for their individual tastes. Based on how the cats like the sushi on offer, the rating of the restaurant as well as the cash reserves rise or fall, therefore high happiness is key for keeping the restaurant open in the endless mode.

<br/>

Based on community voting, our game ranked in the top third of over 3 200 submissions, especially appreciating the humorous writing by placing it in the top 7% in the humour category.

<br/>

Build with the Godot game engine, the game is playable directly in the browser and features a tutorial mode to help with understanding the sushi making mechanics.
