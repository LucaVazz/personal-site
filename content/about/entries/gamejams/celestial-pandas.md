---
tags: gamejam
date: 2024-04-13
title: Celestial Pandas
image: celestial-pandas.png
git: https://gitlab.com/greenopal-studio/celestial-pandas
webGame: https://lucavazz.itch.io/celestial-pandas
keywords:
  - godot
---

[Ludum Dare 55](https://ldjam.com/events/ludum-dare/55/celestial-panda)
