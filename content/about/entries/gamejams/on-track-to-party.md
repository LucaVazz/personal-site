---
tags: gamejam
date: 2022-10-01
title: On Track to Party
image: on-track-to-party.png
git: https://gitlab.com/green-game-17/on-track-to-party
webGame: https://green-game-17.gitlab.io/on-track-to-party
keywords:
  - godot
---

[Ludum Dare 51](https://ldjam.com/events/ludum-dare/51/on-track-to-party/)
