---
tags: gamejam
date: 2020-04-21
title: Project Kepler
image: project-kepler.png
git: https://gitlab.com/green-game-17/project-kepler-ld-46
webGame: https://green-game-17.gitlab.io/project-kepler-ld-46/
keywords:
  - js
  - aframe
---

This game was developed during [Ludum Dare 46](https://ldjam.com/events/ludum-dare/46/project-kepler/). Set only a few years in the future, it plays out an epic journey of humanity to a new home - the exoplanet Kepler 1649c. As the ship's captain, it is your responsibility to keep everything in working order and your fellow passengers happy, to "keep it alive".

<br/>

The community ranked our game in the top 50% of almost 5 000 submissions.

<br/>

Using the WebXR powered A-Frame, the game is fully playable in a web browser and could even be experienced in Virtual Reality, although we didn't finish it enough to allow movement in VR.
