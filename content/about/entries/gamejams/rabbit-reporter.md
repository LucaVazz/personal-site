---
tags: gamejam
date: 2023-09-30
title: Rabbit Reporter
image: rabbit-reporter.png
git: https://gitlab.com/greenopal-studio/rabbit-reporter
webGame: https://lucavazz.itch.io/rabbit-reporter
keywords:
  - godot
---

[Ludum Dare 54](https://ldjam.com/events/ludum-dare/54/rabbit-reporter/)
