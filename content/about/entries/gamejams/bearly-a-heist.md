---
tags: gamejam
date: 2021-12-11
title: Bearly a Heist
image: bearly-a-heist.png
git: https://gitlab.com/green-game-17/bearly-a-heist
webGame: https://lucavazz.itch.io/bearly-a-heist
keywords:
  - twine
---
