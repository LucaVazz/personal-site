---
tags: hackathon
date: 2017-12-16
title: Aquila
image: aquila.png
git: https://github.com/dbhack-aquila/aquila
keywords: 
    - ng
    - bulma
    - py
    - flask
---

During our train ride to the [8th DB Open-Data Hackathon](https://dbmindbox.com/de/db-opendata-hackathons/hackathons/hackathon-8-db-opendata/) a recurring question emerged: "What exactly is the landmark passing by our window?" Based on the track data provided at the Hackathon we set out to create a web-app solving this question.
Travellers get an overview of nearby points of interest and can get further information after tapping on them. The data is provided from Wikidata and Wikivoyage to get great coverage.

<br/>

Our project was chosen as a "Winning Project" and soon after a similar feature was integrated in the on-board ICE portal.

<br/>

The frontend is powered by Angular with styling based on Bulma. It uses a backend written with Flask to receive the aggregated data.
