---
tags: hackathon
date: 2017-10-29
title: Lightning Reads
image: lightning-reads.png
git: https://github.com/Lightning-Reads
keywords: 
    - ng
    - bulma
    - py
    - flask
---

Everyone knows the struggle of researching based on a source text. For the [Campus Hackathon Darmstadt](https://vimeo.com/242559958) we set out to solve this problem, with the help of "Artificial Intelligence meets Open Source". Based on a provided text, our web-app collects the most important words, topics and further information needed to dive into the subject. 

<br/>

The jury awarded our project as the "best result overall".

<br/>

The app's frontend is implemented with Angular and Bulma and provides an easy to grasp representation of the information. These are retrieved from the backend, which is implemented using Python and Flask. It utilizes various natural language processing libraries and an access to Wikipedia, to gather further data.
