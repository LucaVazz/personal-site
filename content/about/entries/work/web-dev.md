---
tags: work
date: 2020-07-17
title: Web-App Developer
image: web-dev.png
keywords: 
    - ng
    - ts
    - scss
---

My main focus as a professional developer is the development of web-apps. Striving to develop responsive, fast and easy to use web-apps while ensuring a high quality developer experience as well is my goal as a lead developer.

Apart from the feature-driven development, I'm also interested in all facets of automation, continuous integration and continuous delivery.

<br>

My expertise is focused on the frontend of web-apps, powered by Angular and TypeScript.
