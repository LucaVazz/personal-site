---
tags: archived_work
date: 2019-03-22
title: App-Developer for Pepper
image: pepper.png
keywords: 
    - kotlin
    - android
    - qisdk
---

As a secondary topic, I'm involved in prototyping and developing apps for Pepper, the humanoid robot. Pepper is interacting with people in a social, intuitive way, opening new use cases for technology to simplify both everyday and new tasks.

While the usage is still in the early stages of adoption, the enhanced possibilities to assist people with this more personal technology is an exiting frontier for me.

<br>

The development is based on the Android SDK with Kotlin and the proprietary QiSDK to control the various special features that Pepper is capable of.
