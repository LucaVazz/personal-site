const definedKeywords = {
  js: {
    color: "--monokai-yellow",
    explanation: "JavaScript",
  },
  ts: {
    color: "--monokai-blue",
    explanation: "TypeScript",
  },
  node: {
    color: "--monokai-green",
    explanation: "Node.js",
  },
  ng1: {
    color: "--monokai-red",
    explanation: "AnuglarJS 1",
  },
  ng: {
    color: "--monokai-red",
    explanation: "Angular",
  },
  vue: {
    color: "--monokai-green",
    explanation: "Vue.js",
  },
  eleventy: {
    color: "--monokai-dark",
    explanation: "Eleventy",
  },
  bulma: {
    color: "--monokai-green",
    explanation: "Bulma",
  },
  sass: {
    color: "--monokai-red",
    explanation: "Sass",
  },
  spring: {
    color: "--monokai-green",
    explanation: "Spring",
  },
  py: {
    color: "--monokai-blue",
    explanation: "Python 3",
  },
  flask: {
    color: "--monokai-blue",
    explanation: "Flask",
  },
  kotlin: {
    color: "--monokai-blue",
    explanation: "Kotlin",
  },
  android: {
    color: "--monokai-green",
    explanation: "Android",
  },
  qisdk: {
    color: "--monokai-blue",
    explanation: "QiSDK",
  },
  sublime: {
    color: "--monokai-orange",
    explanation: "SublimeText 3 Package",
  },
  discord: {
    color: "--monokai-violet",
    explanation: "Discord Bot",
  },
  twitch: {
    color: "--monokai-violet",
    explanation: "Twitch",
  },
  here: {
    color: "--monokai-blue",
    explanation: "HERE Maps",
  },
  mapbox: {
    color: "--monokai-blue",
    explanation: "Mapbox",
  },
  mongo: {
    color: "--monokai-green",
    explanation: "mongoDB",
  },
  aframe: {
    color: "--monokai-red",
    explanation: "A-Frame",
  },
  godot: {
    color: "--monokai-blue",
    explanation: "Godot",
  },
  twine: {
    color: "--monokai-green",
    explanation: "Twine",
  },
  _fallback: {
    color: "--monokai-dark",
    explanation: null,
  },
};

exports.renderKeyword = function (key) {
  let { color, explanation } =
    definedKeywords[key] || definedKeywords["_fallback"];

  let iconElem;
  if (explanation) {
    color = color || definedKeywords._fallback.color;
    iconElem = `<img src="resources/icons/${key}.png"/>`;
  } else {
    explanation = key;
    iconElem = '<i class="la la-cubes"></i>';
  }

  return `
		<div class="keyword" style="--color: var(${color})">
			${iconElem}
			<div class="keyword-explanation">${explanation}</div>
		</div>
	`;
};
