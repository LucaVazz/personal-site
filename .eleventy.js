const renderKeyword = require('./_includes/components/keyword-shortcode.js').renderKeyword


module.exports = function(eleventyConfig) {
	// copy static files:
	eleventyConfig
		.addPassthroughCopy({
			"static-resources":
			"resources"
		})
		.addPassthroughCopy({
			"root-resources":
			"."
		})
		.addPassthroughCopy({
			"node_modules/line-awesome/dist/line-awesome/fonts/la-solid-900.woff2":
			"resources/fonts/la.woff2"
		})
		.addPassthroughCopy({
			"node_modules/line-awesome/dist/line-awesome/fonts/la-solid-900.ttf":
			"resources/fonts/la.ttf"
		})
		.addPassthroughCopy({
			"node_modules/line-awesome/dist/line-awesome/fonts/la-brands-400.woff2":
			"resources/fonts/la-brands.woff2"
		})
		.addPassthroughCopy({
			"node_modules/line-awesome/dist/line-awesome/fonts/la-brands-400.ttf":
			"resources/fonts/la-brands.ttf"
		})
		.addShortcode("keyword", renderKeyword)
	;

	// other config:
	eleventyConfig.setUseGitIgnore(false)

	// custom collection sorting:
	/**
	 * Adds a new collection for the defined sections, ordered by the ordinal of the section's filename instead of its
	 *  date. i.e. 01-about gets sorted before 02-projects
	 */
	function addCollectionForSections(name) {
		const slugToOrdinalNumber = (entry) => +(entry.fileSlug.split('-')[0])
		eleventyConfig.addCollection(`${name}Sections`, function(collection) {
			let sectionsEntries = collection.getFilteredByGlob(`**/${name}/sections/*.md`)
			return sectionsEntries.sort((a, b) => slugToOrdinalNumber(a) - slugToOrdinalNumber(b))
		})
	}

	addCollectionForSections('about')
	addCollectionForSections('cv')
}
